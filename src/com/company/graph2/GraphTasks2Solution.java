package com.company.graph2;

import java.util.HashMap;
import java.util.ArrayList;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        ArrayList<Integer> vertexes = new ArrayList<>();
        int max = Integer.MAX_VALUE;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            hashMap.put(i, max);
        }
        hashMap.put(startIndex, 0);

        while (vertexes.size() != adjacencyMatrix.length) {
            vertexes.add(startIndex);
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if ((adjacencyMatrix[startIndex][j]>0) && (!vertexes.contains(j)) && (j != startIndex)) {
                    hashMap.put(j, Math.min(hashMap.get(j), hashMap.get(startIndex) + adjacencyMatrix[startIndex][j]));
                }
            }
            int min = Integer.MAX_VALUE;
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (hashMap.get(j) < min && j != startIndex && !vertexes.contains(j)) {
                    min = hashMap.get(j);
                    startIndex = j;
                }
            }
        }
        return hashMap;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int weight = 0;
        int vertex = 0;

        ArrayList<Integer> vertexes = new ArrayList<>();
        vertexes.add(vertex);

        while (vertexes.size()!= adjacencyMatrix.length){
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < vertexes.size(); i++) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if ((!vertexes.contains(j)) && (adjacencyMatrix[vertexes.get(i)][j] < min) && (adjacencyMatrix[vertexes.get(i)][j]!=0)) {
                        min = adjacencyMatrix[vertexes.get(i)][j];
                        vertex = j;
                    }
                }
            }
            weight += min;
            vertexes.add(vertex);
        }
        return weight;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
